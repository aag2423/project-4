/******************************************************************/
/*         Geometry functions                                     */
/*                                                                */
/* Group Members: Anthony Garza, Nicholas Carter                  */
/******************************************************************/

#ifdef _WIN32
#include <windows.h>
#endif
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "raytrace.h"

point* makePoint(GLfloat x, GLfloat y, GLfloat z) {
  point* p;
  /* allocate memory */
  p = (point*) malloc(sizeof(point));
  /* put stuff in it */
  p->x = x; p->y = y; p->z = z; 
  p->w = 1.0;
  return (p);
}

/* makes copy of point (or vector) */
point* copyPoint(point *p0) {
  point* p;
  /* allocate memory */
  p = (point*) malloc(sizeof(point));

  p->x = p0->x;
  p->y = p0->y;
  p->z = p0->z;
  p->w = p0->w;         /* copies over vector or point status */
  return (p);
}

/* unallocates a point */
void freePoint(point *p) {
  if (p != NULL) {
    free(p);
  }
}

/* vector from point p to point q is returned in v */
void calculateDirection(point* p, point* q, point* v) {
  v->x = q->x - p->x;
  v->y = q->y - p->y;
  v->z = q->z - p->z;
  /* a direction is a point at infinity */
  v->w = 0.0;
  GLfloat vsqrt = sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
  v->x /= vsqrt;
  v->y /= vsqrt;
  v->z /= vsqrt;
}

/* given a vector, sets its contents to unit length */
void normalize(vector* v) {
	/* PUT YOUR CODE HERE */
	double s = sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
	v->x /= s;
	v->y /= s;
	v->z /= s;
}

/* point on ray r parameterized by t is returned in p */
void findPointOnRay(ray* r,double t,point* p) {
  p->x = r->start->x + t * r->dir->x;
  p->y = r->start->y + t * r->dir->y;
  p->z = r->start->z + t * r->dir->z;
  p->w = 1.0;
  
}


/* SPHERES */

sphere* makeSphere(GLfloat x, GLfloat y, GLfloat z, GLfloat r) {
  sphere* s;
  /* allocate memory */
  s = (sphere*) malloc(sizeof(sphere));

  /* put stuff in it */
  s->c = makePoint(x,y,z);   /* center */
  s->r = r;   /* radius */
  s->m = NULL;   /* material */
  return(s);
}

/* returns TRUE if ray r hits sphere s, with parameter value in t */
int raySphereIntersect(ray* r,sphere* s,double* t) {
  point p;   /* start of transformed ray */
  double a,b,c;  /* coefficients of quadratic equation */
  double D;    /* discriminant */
  point* v;
  
  /* transform ray so that sphere center is at origin */
  /* don't use matrix, just translate! */
  p.x = r->start->x - s->c->x;
  p.y = r->start->y - s->c->y;
  p.z = r->start->z - s->c->z;
  v = r->dir; /* point to direction vector */


  a = v->x * v->x  +  v->y * v->y  +  v->z * v->z;
  b = 2*( v->x * p.x  +  v->y * p.y  +  v->z * p.z);
  c = p.x * p.x + p.y * p.y + p.z * p.z - s->r * s->r;

  D = b * b - 4 * a * c;
  
  if (D < 0) {  /* no intersection */
    return (FALSE);
  }
  else {
    D = sqrt(D);
    /* First check the root with the lower value of t: */
    /* this one, since D is positive */
    *t = (-b - D) / (2*a);
    /* ignore roots which are less than zero (behind viewpoint) */
    if (*t < 0) {
      *t = (-b + D) / (2*a);
    }
    if (*t < 0) { return(FALSE); }
    else return(TRUE);
  }
}

/* normal vector of s at p is returned in n */
/* note: dividing by radius normalizes */
void findSphereNormal(sphere* s, point* p, vector* n) {
  n->x = (p->x - s->c->x) / s->r;  
  n->y = (p->y - s->c->y) / s->r;
  n->z = (p->z - s->c->z) / s->r;
  n->w = 0.0;
}

/* PLANES */
plane* makePlane(GLfloat x1, GLfloat y1, GLfloat z1, GLfloat x2, GLfloat y2, GLfloat z2, GLfloat x3, GLfloat y3, GLfloat z3)
{
  plane* p = (plane*) malloc(sizeof(plane));
  p->p1 = makePoint(x1, y1, z1);
  p->p2 = makePoint(x2, y2, z2);
  p->p3 = makePoint(x3, y3, z3);
  
  p->n = (vector*) malloc(sizeof(vector));
  GLfloat x21 = x2 - x1;
  GLfloat y21 = y2 - y1;
  GLfloat z21 = z2 - z1;
  
  GLfloat x31 = x3 - x1;
  GLfloat y31 = y3 - y1;
  GLfloat z31 = z3 - z1;
  
  p->n->x = y21*z31 - z21*y31;
  p->n->y = z21*x31 - x21*z31;
  p->n->z = x21*y31 - y21*x31;
  
  normalize(p->n);
  p->c = p->n->x * p->p1->x + p->n->y * p->p1->y + p->n->z * p->p1->z;  
  return(p);
}

int rayPlaneIntersect(ray* r, plane* p, double* t)
{
	/*
	double x =(p->p1->x)-(r->start->x);
	double y =(p->p1->y)-(r->start->y);
	double z =(p->p1->z)-(r->start->z);
	
	
	 vector* n1 = p->n;
	// point p2;
	// double a = p->c + (n1->x * r->start->x + n1->y*r->start->y + n1->z*r->start->z);
	 //double b = n1->x*r->dir->x + n1->y*r->dir->y + n1->z*r->dir->z;
	 
	 double top = (n1->x * x + n1->y * y + n1->z * z);
	 double b = r->start->x * r->dir->x + r->start->y * r->dir->y + r->start->z * r->dir->z;
	 if(b == 0)
	 	return FALSE;
	 *t = top/b;
	 if( *t < 0)
	 	return FALSE;
	 
	 return TRUE;
	 */
	 
	 vector* n1 = p->n;
	 point w;
	 double a = p->c + (n1->x * r->start->x + n1->y*r->start->y + n1->z*r->start->z);
	 double b = n1->x*r->dir->x + n1->y*r->dir->y + n1->z*r->dir->z;
	 
	 if(b == 0)
	 	return FALSE;
	 *t = a/b;
	 if( *t < 0)
	 	return FALSE;
	 
	 findPointOnRay(r, *t, &w);
	 if(w.y > 0)
	 	return FALSE; 
	 return TRUE;
}


/* cylinder stuff */
cylinder* makeCylinder(GLfloat x, GLfloat y, GLfloat z, GLfloat r, GLfloat h)
{
	cylinder* c = (cylinder*) malloc(sizeof(cylinder));
	c->center = makePoint(x,y,z);
	c->r = r;
	c->h = h;
	c->m=NULL;
	return(c);
}

int rayCylinderIntersect(ray* r, cylinder* c, double* t)
{
	point p, w;
	point* v;
	p.x = r->start->x - c->center->x;
  	p.y = r->start->y - c->center->y;
  	p.z = r->start->z - c->center->z;
  	v = r->dir;

	double b = 2*(p.x*v->x)+2*(p.z*v->z);
	double a = (v->x*v->x)+(v->z*v->z);
	double c1 = (p.x*p.x)+(p.z*p.z)-c->r;
	double d = b*b - 4*a*c1;

	
  if (d < 0) {  /* no intersection */
    return (FALSE);
  }else{
	 d = sqrt(d);
    /* First check the root with the lower value of t: */
    /* this one, since D is positive */
    *t = (-b - d) / (2*a);
    /* ignore roots which are less than zero (behind viewpoint) */
    if (*t < 0) {
      *t = (-b + d) / (2*a);
    }
    if (*t < 0) { 
    	return(FALSE); 
    }
    else
    { 
    	findPointOnRay(r, *t, &w);
    	//check if w is within the cylinder
    	if(cylindercheck(w,c))
    		return(TRUE);
    	return FALSE;
    }
  }
    
}

bool cylindercheck(point w, cylinder* c){
	if((w.y <= (c->center->y+c->h)) && (w.y >= (c->center->y - c->h) ) /*&& (w.z <= (c->center->z + c->r)) && (w.z >= (c->center->z - c->r))*/)
		return true;
	else
		return false;
}

void findCylinderNormal(cylinder* c, point* p, vector* n)
{
	n->x = (p->x - c->center->x)/c->r;
	n->y = 1/c->r;
	n->z = (p->z - c->center->z)/c->r;
	//double dotA=n->y;
	
	
	normalize(n);
	n->w = 0.0;
}
