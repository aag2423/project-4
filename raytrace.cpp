/******************************************************************/
/*         Main raytracer file                                    */
/*                                                                */
/* Group Members: Anthony Garza, Nicholas Carter                  */
/******************************************************************/

#ifdef _WIN32
#include <windows.h>
#endif
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "lowlevel.h"
#include "raytrace.h"

/* local functions */
void initScene(void);
void initCamera (int, int);
void display(void);
void init(int, int);
void traceRay(ray*,color*,int);
void drawScene(void);


/* local data */

/* the scene: so far, just one sphere */
sphere* s[4];
plane* planes[2];
light* lights[2];
cylinder* cyl;

/* the viewing parameters: */
point* viewpoint;
GLfloat pnear;  /* distance from viewpoint to image plane */
GLfloat fovx;  /* x-angle of view frustum */
int width = 500;     /* width of window in pixels */
int height = 500;    /* height of window in pixels */

int main (int argc, char** argv) {
  int win;

  glutInit(&argc,argv);
  glutInitWindowSize(width,height);
  glutInitWindowPosition(100,100);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  win = glutCreateWindow("raytrace");
  glutSetWindow(win);
  init(width,height);
  glutDisplayFunc(display);
  glutMainLoop();
  return 0;
}

void init(int w, int h) {

  /* OpenGL setup */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
  glClearColor(0.0, 0.0, 0.0, 0.0);  

  /* low-level graphics setup */
  initCanvas(w,h);

  /* raytracer setup */
  initCamera(w,h);
  initScene();
}

void display() {
  glClear(GL_COLOR_BUFFER_BIT);
  drawScene();  /* draws the picture in the canvas */
  flushCanvas();  /* draw the canvas to the OpenGL window */
  glFlush();
}

void initScene () {

  lights[0] = makeLight(3.0, 1.0, -2.0, 1, 1, 1, 0.6);
  lights[0]->m = makeMaterial(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1);
  lights[1] = makeLight(-2, 3.0, 0.0, 1, 1, 1, 0.3);
  lights[1]->m = makeMaterial(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1);

	/* red sphere */
  s[0] = makeSphere(0.3,0.15,-2.0,0.05);
  s[0]->m = makeMaterial(1.0,0.1,0.15,0.7, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 50);

	/* blue sphere */
  s[1] = makeSphere(-0.3,0.0,-2.0,0.15);
  s[1]->m = makeMaterial(0.0,0.65,1.0,0.7, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 50);
  
   /* green sphere */
  s[2] = makeSphere(0.15,0.0,-2.0,0.1);
  s[2]->m = makeMaterial(0.0,1.0,0.0,0.7, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 50);
  
  s[3] = makeSphere(0.0, -9.6, -3.0, 9.5);
  s[3]->m = makeMaterial(0.83, 0.78, 0.79, 0.7, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 50);
  
  
  cyl = makeCylinder(0.0, 0.0, -3.5, 0.005, 0.2);
  cyl->m = makeMaterial(1.0, 0.33, 0.05, 0.7, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 10);
  
  planes[0] = makePlane(2.0, -0.20000001, -1.0, 2.0, -0.2, -2.0, 0.0,-0.2, -2.0);
  planes[0]->m = makeMaterial(1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 50);  

}

void initCamera (int w, int h) {
  viewpoint = makePoint(0.0,0.0,0.0);
  pnear = 1.0;
  fovx = PI/6;
}

void black(color* c){
	c->r = 0.0;
	c->g = 0.0;
	c->b = 0.0;
}

void drawScene () {

  int i,j;
  GLfloat imageWidth;
  /* declare data structures on stack to avoid dynamic allocation */
  point worldPix;  /* current pixel in world coordinates */
  point direction; 
  ray r, r2, r3, r4, r5, r6, r7, r8, r9;
  color c, c2, c3, c4, c5, c6, c7, c8, c9;
  bool * h = (bool*) malloc(sizeof(bool));
  *h = false;
  /* initialize */
  worldPix.w = 1.0;
  worldPix.z = -pnear;

  r.start = &worldPix;
  r.dir= &direction;
  r2.start = &worldPix;
  r2.dir= &direction;
  r3.start = &worldPix;
  r3.dir= &direction;
  r4.start = &worldPix;
  r4.dir= &direction;
  r5.start = &worldPix;
  r5.dir= &direction;
  r6.start = &worldPix;
  r6.dir= &direction;
  r7.start = &worldPix;
  r7.dir= &direction;
  r8.start = &worldPix;
  r8.dir= &direction;
  r9.start = &worldPix;
  r9.dir= &direction;



  imageWidth = 2*pnear*tan(fovx/2);


	  /* trace a ray for every pixel */
	  for (i=0; i<width; i++) {
		/* Refresh the display */
		/* Comment this line out after debugging */
		// 

		for (j=0; j<height; j++) {
			

		  /* find position of pixel in world coordinates */
		  /* y position = (pixel height/middle) scaled to world coords */ 
		  worldPix.y = (j-(height/2))*imageWidth/width;
		  /* x position = (pixel width/middle) scaled to world coords */ 
		  worldPix.x = (i-(width/2))*imageWidth/width;

		  /* find direction */
		  /* direction is normalized */
		  calculateDirection(viewpoint,&worldPix,&direction);

		  /* trace the ray! */
		  
		  traceRay(&r,&c,1, h);
		 r2.start->x +=  0.00095;
		  traceRay(&r2, &c2,1, h);
		  r3.start->y += 0.00095;
		  traceRay(&r3, &c3,1, h);
		  r4.start->x -= 0.00095;
		  traceRay(&r4, &c4,1, h);
	  	  r5.start->x -= 0.00095;
		  traceRay(&r5, &c5,1, h);
		  r6.start->y -= 0.00095;
		  traceRay(&r6, &c6,1, h);
		  r7.start->y -= 0.00095;
		  traceRay(&r7, &c7,1, h);
		  r8.start->x += 0.00095;
		  traceRay(&r8, &c8,1, h);
		  r9.start->x += 0.00095;
		  traceRay(&r9, &c9,1, h);
		  c.r +=c.r+c2.r+ c3.r + c4.r + c5.r + c6.r + c7.r + c8.r + c9.r;
		  c.g +=c.g+ c2.g + c3.g + c4.g + c5.g + c6.g + c7.g + c8.g + c9.g;
		  c.b +=c.b+ c2.b + c3.b + c4.b + c5.b + c6.b + c7.b + c8.b + c9.b;
		  c.r /=10; 
		  c.g /=10;
		  c.b /=10;
		  		//
		  /* write the pixel! */
		  drawPixel(i,j,c.r,c.g,c.b);
		}
	  }
}

/* returns the color seen by ray r in parameter c */
/* d is the recursive depth */
void traceRay(ray* r, color* c, int d, bool* hit) {
  point p;  /* first intersection point */
  vector n;
  material* m;
  color* dc = (color*) malloc(sizeof(color));
  p.z = FLT_MAX;
  p.w = 0.0;  /* inialize to "no intersection" */
  firstHit(r,&p,&n,&m);
  
  if (p.w != 0.0) {
  	if(d > 0)
  	{
  		//printf("What is d: %i \n",d);
  		ray ref;
  		point *temp = makePoint(p.x, p.y, p.z); 
  		ref.start = temp;
  		ref.dir = (vector*) malloc(sizeof(vector));
  		double dot = r->dir->x*n.x + r->dir->y*n.y + r->dir->z*n.z;
  		double ax = 2*n.x*dot;
  		double ay = 2*n.y*dot;
  		double az = 2*n.z*dot;
  		ref.dir->x = r->dir->x - ax;
  		ref.dir->y = r->dir->y - ay;
  		ref.dir->z = r->dir->z - az;
  		normalize(ref.dir);
  		ref.start->x += EPSILON * ref.dir->x;
  		ref.start->y += EPSILON * ref.dir->y;
  		ref.start->z += EPSILON * ref.dir->z;
  		//trace reflected ray
  		traceRay(&ref, dc, d-1, hit);
  		shade(&p, &n, m, r->dir, c, d-1);
  		c->r += dc->r;
  		c->g += dc->g;
  		c->b += dc->b;
  		if(*hit)
  		{
  			c->r /= 2.0;
  			c->g /= 2.0;
  			c->b /= 2.0;
  		}
  		delete temp;
  	}
  	else
  	{
  		
		shade(&p,&n,m,r->dir,c,d);  /* do the lighting calculations */
		*hit = true;
	}
  }
  else
  {
  	c->r = 0.0;
  	c->g = 0.0;
  	c->b = 0.0;
  	*hit = false;
  }
}

/* firstHit */
/* If something is hit, returns the finite intersection point p, 
   the normal vector n to the surface at that point, and the surface
   material m. If no hit, returns an infinite point (p->w = 0.0) */
void firstHit(ray* r, point* p, vector* n, material* *m) {
	double t = DBL_MAX;     /* parameter value at first hit */
	int hit = FALSE;
	int past_hit = FALSE;
	double min_t = DBL_MAX;
	for(int i = 3; i >= 0; i--)
	{
	  hit = raySphereIntersect(r,s[i],&t);
	  if (hit && t < min_t) {
	  	min_t = t;
		*m = s[i]->m;
		findPointOnRay(r,t,p);
		findSphereNormal(s[i],p,n);
		past_hit = TRUE;
	  } else if(!past_hit) {
		/* indicates no hit */
		p->w = 0.0;
	  }
	}
	
	hit = rayCylinderIntersect(r, cyl, &t);
	if(hit && t < min_t){
		min_t = t;
		*m = cyl->m;
		findPointOnRay(r,t,p);
		findCylinderNormal(cyl, p, n);
		past_hit = TRUE;
	}
	else if(!past_hit){
		p->w = 0.0;
	}
	/*
	hit = rayPlaneIntersect(r, planes[0], &t);
	if(hit && t < min_t)
	{
		*m = planes[0]->m;
		findPointOnRay(r, t, p);
		n = planes[0]->n; 
		min_t = t;
		past_hit = TRUE;
	}
	else if(!past_hit){
		p->w = 0.0;
	}
	*/
}
