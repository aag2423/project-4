#include <cfloat>

#ifndef _RAYTRACE_H_
#define _RAYTRACE_H_

/******************************************************************/
/*         Raytracer declarations                                 */
/******************************************************************/


/* constants */
#define TRUE 1
#define FALSE 0
#define NUM_LIGHTS 1
#define PI 3.14159265358979323846264338327
#define EPSILON 0.0001

/* data structures */
typedef struct point {
  GLfloat x;
  GLfloat y;
  GLfloat z;
  GLfloat w;
  
  bool operator==(const point& rhs){
  	return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
  }
} point;


/* a vector is just a point */
typedef point vector;

/* a ray is a start point and a direction */
typedef struct ray {
  point* start;
  vector* dir;
} ray;

typedef struct material {
  /* color */
  GLfloat r;
  GLfloat g;
  GLfloat b;
  
   
  /* ambient reflectivity */
  GLfloat amb;
  /* diffused reflectivity */
  GLfloat kr, kg, kb;
  /* specular reflectivity */ 
  GLfloat ksr, ksg, ksb, shiny;
  GLfloat trans;
} material;

typedef struct color {
  GLfloat r;
  GLfloat g;
  GLfloat b; 
  /* these should be between 0 and 1 */
} color;

typedef struct sphere {
  point* c;  /* center */
  GLfloat r;  /* radius */
  material* m;
} sphere;

typedef struct light {
	point* src;
	sphere* s;
	material* m;
	color* c;
	GLfloat intensity;
} light;

typedef struct plane {
	point* p1;
	point* p2;
	point* p3;
	GLfloat c;
	vector* n;
	material* m;
} plane;

typedef struct cylinder {
	point* center;
	GLfloat r, h;
	material* m;
} cylinder;

/* functions in raytrace.cpp */
void traceRay(ray*, color*, int, bool*);
void firstHit(ray*,point*,vector*,material**);

/* functions in geometry.cpp */
sphere* makeSphere(GLfloat, GLfloat, GLfloat, GLfloat);
plane* makePlane(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat); 
point* makePoint(GLfloat, GLfloat, GLfloat);
point* copyPoint(point *);
void freePoint(point *);
void normalize(vector* v);
void calculateDirection(point*,point*,point*);
void findPointOnRay(ray*,double,point*);
int raySphereIntersect(ray*,sphere*,double*);
void findSphereNormal(sphere*,point*,vector*);
int rayPlaneIntersect(ray*, plane*, double*);
cylinder* makeCylinder(GLfloat x, GLfloat y, GLfloat z, GLfloat r, GLfloat h);
int rayCylinderIntersect(ray* r, cylinder* c, double* t);
void findCylinderNormal(cylinder* c, point* p, vector* n);
bool cylindercheck(point w, cylinder* c);


/* functions in light.cpp */
material* makeMaterial(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat,GLfloat,GLfloat,GLfloat);
light* makeLight(GLfloat x, GLfloat y, GLfloat z, GLfloat r, GLfloat g, GLfloat b, GLfloat i);
void shade(point*,vector*,material*,vector*,color*,int);
bool isShadow(ray*, point*);

/* global variables */
extern int width;
extern int height;
extern light* lights[];
extern sphere* s[];
extern cylinder* cyl;
extern plane* planes[]; 

#endif	/* _RAYTRACE_H_ */
