/******************************************************************/
/*         Lighting functions                                     */
/*                                                                */
/* Group Members: Anthony Garza, Nicholas Carter                  */
/******************************************************************/

#ifdef _WIN32
#include <windows.h>
#endif
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "common.h"
#include "raytrace.h"
#include <algorithm>


material* makeMaterial(GLfloat r, GLfloat g, GLfloat b, GLfloat amb, 
						GLfloat kr, GLfloat kg, GLfloat kb, 
						GLfloat ksr, GLfloat ksg, GLfloat ksb, GLfloat shiny) {
  material* m;
  
  /* allocate memory */
  m = (material*) malloc(sizeof(material));
  /* put stuff in it */
  m->r = r;
  m->g = g;
  m->b = b;
  m->amb = amb;
  m->kr = kr;
  m->kg = kg;
  m->kb = kb;
  m->ksr = ksr;
  m->ksg = ksg;
  m->ksb = ksb;
  m->shiny = shiny;
  return(m);
}

light* makeLight(GLfloat x, GLfloat y, GLfloat z, GLfloat r, GLfloat g, GLfloat b, GLfloat i)
{
	light* l; 
	l = (light*) malloc(sizeof(light));
	l->src = makePoint(x, y, z);
	l->c = (color*) malloc(sizeof(color));
	(l->c)->r = r;
	(l->c)->g = g;
	(l->c)->b = b;
	l->s = makeSphere(x,y,z,0.001);
	l->intensity = i;
	return(l);
}

/* LIGHTING CALCULATIONS */


/* shade */
/* color of point p with normal vector n and material m returned in c */
/* in is the direction of the incoming ray and d is the recusive depth */
void shade(point* p, vector* n, material* m, vector* in, color* c, int d) {

  ray ptol;
  point temp_EP;
  ptol.start = &temp_EP;
  vector ptol_dir;
  ptol.dir = &ptol_dir;
  
  /* so far, just finds ambient component of color */
  c->r = m->amb * m->r;
  c->g = m->amb * m->g;
  c->b = m->amb * m->b;
  
  
  for(int i =0; i < 2; i++)
  {	
  	GLfloat lx, ly, lz;
	lx = (lights[i]->src)->x - p->x;
	ly = (lights[i]->src)->y - p->y;
	lz = (lights[i]->src)->z - p->z;
	GLfloat lsqrt = sqrt(lx*lx + ly*ly + lz*lz);
	lx /= lsqrt;
	ly /= lsqrt;
	ly /= lsqrt;

	/* diffuse component */
	GLfloat dot = (lx*n->x + ly*n->y + lz*n->z);
	c->r += m->kr * std::max((GLfloat)0.0, dot) * (lights[i]->c)->r * lights[i]->intensity;  
	c->g += m->kg * std::max((GLfloat)0.0, dot) * (lights[i]->c)->g * lights[i]->intensity;
	c->b += m->kb * std::max((GLfloat)0.0, dot) * (lights[i]->c)->b * lights[i]->intensity;
    
    if(dot > 0)
    {
		calculateDirection(p,lights[i]->src, ptol.dir);
		ptol.start->x = p->x + EPSILON * ptol.dir->x;
	   	ptol.start->y = p->y + EPSILON * ptol.dir->y;
	   	ptol.start->z = p->z + EPSILON * ptol.dir->z;
		
	   	if(!isShadow(&ptol, p))
	   	{	
			/* specular component */
			GLfloat hx = lx - in->x;
			GLfloat hy = ly - in->y;
			GLfloat hz = lz - in->z;
		
			GLfloat hsqrt = sqrt(hx*hx + hy*hy + hz*hz);
			hx /= hsqrt;
			hy /= hsqrt;
			hz /= hsqrt;
		
		   	GLfloat sdot = hx*n->x + hy*n->y + hz*n->z;
		   	sdot = pow(sdot, m->shiny);
		   	c->r += m->ksr * sdot;
		   	c->g += m->ksg * sdot;
		   	c->b += m->ksb * sdot;
		}
		else
		{
			c->r *= (1 - dot);
			c->g *= (1 - dot);
			c->b *= (1- dot);
		}
    }
  }
  
  /* clamp color values to 1.0 */
  if (c->r > 1.0) c->r = 1.0;
  else if(c->r < 0.0)c->r =0.0;
  
  if (c->g > 1.0) c->g = 1.0;
  else if(c->g < 0.0) c->g=0.0;
  if (c->b > 1.0) c->b = 1.0;
   else if(c->b < 0.0) c->b=0.0;
}

bool isShadow(ray* r, point* p)
{
	double t = DBL_MAX;
	
	if(rayCylinderIntersect(r, cyl,&t)){
			return true;
	}
	if(rayPlaneIntersect(r,planes[0],&t)){
		return true;
	}
	for(int i = 0; i < 3; i++)
	{
		if(raySphereIntersect(r, s[i], &t))
		{
			//printf("isShadow return true \n");
			return true;
		}
	}
	return false;
}

